<?php
/**
 * Created by PhpStorm.
 * User: Mario Figueroa
 * Date: 06/12/2016
 * Time: 12:03
 */

namespace Tmwk\SparkPostBundle\DependencyInjection;

use SparkPost\SparkPost as Spp;
use GuzzleHttp\Client;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SparkPost
{
    private $spp;
    private $container;
    private $recipients = array();
    private $cc         = array();
    private $bcc        = array();
    private $from       = array();
    private $subject;
    private $body_html;
    private $body_text;
    private $parameters;
    private $result_code;
    private $result_message;
    private $total_rejected_recipients;
    private $total_accepted_recipients;
    private $transmission_id;


    /**
     * SparkPost constructor.
     * @param ContainerInterface $container
     * @param $key
     */
    public function __construct(ContainerInterface $container, $key)
    {
        $this->container = $container;
        $httpClient = new GuzzleAdapter(new Client());
        $this->spp = new Spp($httpClient, ['key' => $key]);
    }

    /**
     * @return bool
     */
    public function send()
    {
        $options = array(
            'content'           => [
                'from'    => $this->from,
                'subject' => $this->subject,
                'html'    => $this->body_html,
                'text'    => $this->body_text,
            ],
            'substitution_data' => $this->parameters,
            'recipients'        => $this->recipients,
            'cc'                => !empty($this->cc) ? $this->cc : null,
            'bcc'               => !empty($this->bcc) ? $this->bcc : null,
        );

        $promise = $this->spp->transmissions->post($options);
        try {
            $response = $promise->wait();
            $this->result_code = $response->getStatusCode();
            $this->result_message = 'Success';
            $result = $response->getBody();
            $this->total_rejected_recipients = $result['results']['total_rejected_recipients'];
            $this->total_accepted_recipients = $result['results']['total_accepted_recipients'];
            $this->transmission_id = $result['results']['id'];
            return true;
        } catch (\Exception $e) {
            $this->result_code = $e->getCode();
            $this->result_message = $e->getMessage();
            return false;
        }
    }

    /**
     * @param $email
     * @param $name
     * @return $this
     */
    public function addRecipient($email, $name)
    {
        array_push($this->recipients, array('address' => array('name' => $name, 'email' => $email)));
        return $this;
    }

    /**
     * @param $email
     * @param $name
     * @return $this
     */
    public function addCc($email, $name)
    {
        array_push($this->cc, array('address' => array('name' => $name, 'email' => $email)));
        return $this;
    }

    /**
     * @param $email
     * @param $name
     * @return $this
     */
    public function addBcc($email, $name)
    {
        array_push($this->bcc, array('address' => array('name' => $name, 'email' => $email)));
        return $this;
    }

    /**
     * @param $email
     * @param $name
     * @return $this
     */
    public function from($email, $name)
    {
        $this->from = array(
            'name'  => $name,
            'email' => $email,
        );
        return $this;
    }

    /**
     * @param $subject
     * @return $this
     */
    public function subject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param $body
     * @return $this
     */
    public function bodyHtml($body)
    {
        $this->body_html = $body;
        return $this;
    }

    /**
     * @param $body
     * @return $this
     */
    public function bodyText($body)
    {
        $this->body_text = $body;
        return $this;
    }

    /**
     * @param $view
     * @param array $parameters
     * @return $this
     */
    public function bodyRender($view, array $parameters = array())
    {
        if ($this->container->has('templating')) {
            $this->body_html = $this->container->get('templating')->render($view, $parameters);
        }

        if (!$this->container->has('twig')) {
            throw new \LogicException('You can not use the "renderView" method if the Templating Component or the Twig Bundle are not available.');
        }

        $this->body_html = $this->container->get('twig')->render($view, $parameters);
        return $this;
    }

    /**
     * @param array $parameters
     * @return $this
     */
    public function parameteres(array $parameters = array())
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResultCode()
    {
        return $this->result_code;
    }

    /**
     * @return mixed
     */
    public function getResultMessage()
    {
        return $this->result_message;
    }

    /**
     * @return mixed
     */
    public function getTotalRejectedRecipients()
    {
        return $this->total_rejected_recipients;
    }

    /**
     * @return mixed
     */
    public function getTotalAcceptedRecipients()
    {
        return $this->total_accepted_recipients;
    }

    /**
     * @return mixed
     */
    public function getTransmissionId()
    {
        return $this->transmission_id;
    }


}