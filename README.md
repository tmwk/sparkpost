#SparkPost Symfony Bundle

##Requerimientos

- [Symfony 2.8+](https://github.com/Kdyby/Curl)
- [guzzlehttp/guzzle](https://packagist.org/packages/guzzlehttp/guzzle)
- [php-http/guzzle6-adapter](https://github.com/php-http/guzzle6-adapter)
- [sparkpost/sparkpost](https://github.com/SparkPost/php-sparkpost)

##Instalación

    composer require tmwk/sparkpost
    
Modifica el archivo app/AppKernel.php para registrar el nuevo Bundle
    
    $bundles = array(
        // ...
        new Tmwk\SparkPostBundle\TmwkSparkPostBundle(),
        // ...
    );
    
Agrega a tu archivo app/config/config.yml lo siguiente:

    tmwk_spark_post:
        api_token: <inserta el api key de SparkPost>
           

##Uso

###Ejemplo envió normal
    $spp = $this->get('tmwk.sparkpost');
    $spp
        ->from('from@suemail.com', 'Johnny Test')
        ->addRecipient('receiver@suemail.com', 'Juan Perez')
        ->subject('Prueba de correo usando SparkPost')
        ->bodyHtml('<html><body><h1>Congratulations, {{name}}!</h1><p>You just sent your very first mailing!</p></body></html>')
        ->parameteres(array('name' => 'Carlos Peña'))
    ;    
    
    if ($spp->send()) {
        //Se ejecuta si se envio correctamente.
    } else {
        //Se ejecuta si fallo el envio
    }
   
    echo $spp->getResultCode() . "\n";
    echo $spp->getResultMessage() . "\n";

###Ejemplo envió con plantilla twig
    
    $spp = $this->get('tmwk.sparkpost');
    $spp
        ->from('from@suemail.com', 'Johnny Test')
        ->addRecipient('receiver@suemail.com', 'Juan Perez')
        ->subject('Prueba de correo usando SparkPost')
        ->bodyRender(':default:index.html.twig')
    ;    
    
    if ($spp->send()) {
        //Se ejecuta si se envio correctamente.
    } else {
        //Se ejecuta si fallo el envio
    }
   
    echo $spp->getResultCode() . "\n";
    echo $spp->getResultMessage() . "\n";
        
###Ejemplo envió con copia

    $spp = $this->get('tmwk.sparkpost');
    $spp
        ->from('from@suemail.com', 'Johnny Test')
        ->addRecipient('receiver@suemail.com', 'Juan Perez')
        ->subject('Prueba de correo usando SparkPost')
        ->bodyRender(':default:index.html.twig')
        ->addCc('copia@suemail.com', 'Mario Test')
    ;    
    
    if ($spp->send()) {
        //Se ejecuta si se envio correctamente.
    } else {
        //Se ejecuta si fallo el envio
    }
   
    echo $spp->getResultCode() . "\n";
    echo $spp->getResultMessage() . "\n";
    
###Ejemplo envió con copia oculta

    $spp = $this->get('tmwk.sparkpost');
    $spp
        ->from('from@suemail.com', 'Johnny Test')
        ->addRecipient('receiver@suemail.com', 'Juan Perez')
        ->subject('Prueba de correo usando SparkPost')
        ->bodyRender(':default:index.html.twig')
        ->addBcc('copia@suemail.com', 'Mario Test')
    ;    
    
    if ($spp->send()) {
        //Se ejecuta si se envio correctamente.
    } else {
        //Se ejecuta si fallo el envio
    }
   
    echo $spp->getResultCode() . "\n";
    echo $spp->getResultMessage() . "\n";
    
##Métodos

- `->from()`: Define quien envía el mensaje. {mail, nombre}
- `->addRecipient()`: Define quien recibe el mensaje. {mail, nombre} "puede ser uno o mas"
- `->subject()`: Indica el asunto del mensaje
- `->bodyHtml()`: Define el cuerpo del mensaje en formato HTML
- `->bodyText()`: Define el cuerpo del mensaje en formato Texto
- `->bodyRender()`: Obtiene el cuerpo HTML desde una plantilla Twig
- `->addCc()`: Envía una copia al receptor indicado {mail, nombre} "puede ser uno o mas" 
- `->addBcc()`: Envía una copia oculta al receptor indicado {mail, nombre} "puede ser uno o mas"
- `->parameteres()`: Envía un arreglo con parámetros al cuerpo del mensaje {array('nombre parámetro', 'valor')}, los parámetro se pueden leer usando {{nombre parámetro}}